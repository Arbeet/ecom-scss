import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Categories from "./components/categories/Categories"
import Home from './home/Home'
import Header from "./components/header/Header"
import AppContext from './utils/context';
import Products from './components/Products/Products';
import Category from './components/Category/Category';
import SinglePrduct from './components/SingleProduct/SinglePrduct';

function App() {
  return (
    <div>
      <BrowserRouter>
      <AppContext />
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/category/:id'  element={<Category />}/>
        <Route path='/product/:id' element={<Products /> } />
        <Route path='/single-product/:id' element={<SinglePrduct /> }/>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
