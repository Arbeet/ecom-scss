import React from 'react'
import "./CartItem.scss"
import { MdClose } from 'react-icons/md'

function CartItem() {
  return (
    <div className="cart-products">
        <div className="cart-product">
            <div className="img-container">
                <img src="assets/products/headphone-prod-2.webp" alt="" />
            </div>
            <div className="prod-details">
                <span className="name">Product Name</span>
                <MdClose className='close-btn'/>
                <div className="quantity-buttons">
                    <span>-</span>
                    <span>5</span>
                    <span>+</span>
                </div>
                <div className="text">
                    <span>3</span>
                    <span>*</span>
                    <span>500</span>
                </div>
            </div>
        </div>
    </div>
  )
}

export default CartItem
