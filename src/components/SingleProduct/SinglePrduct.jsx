import React from 'react'
import "./SingleProduct.scss"
import {FaCartPlus, FaFacebookF, FaInstagram, FaTwitter} from 'react-icons/fa'
import RelatedProducts from './Related/RelatedProducts'

function SinglePrduct() {
  return (
    <div className='single-product-main-content'>
        <div className="layout">
            <div className="single-product-page">
                <div className="left">
                    <img src="/assets/products/headphone-prod-1.webp" alt="" />
                </div>
                <div className="right">
                    <span className="name">Name</span>
                    <span className="price">5000</span>
                    <span className='desc'>Product Description</span>

                    <div className="cart-buttons">
                        <div className="quantity-buttons">
                            <span>-</span>
                            <span>5</span>
                            <span>+</span>
                        </div>
                        <button className='add-to-cart-button'>
                        <FaCartPlus size={20}/>
                            ADD TO CART</button>
                       
                    </div>
                    <span className='divider'/>
                    <div className="info-item">
                        <span className="text-bold">
                            Category:
                        <span> Headphones</span>
                        </span>
                        <span className='text-bold'>
                            Share:
                            <span>
                                <FaFacebookF />
                                <FaTwitter />
                                <FaInstagram />
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <RelatedProducts />
        </div>
    </div>
  )
}

export default SinglePrduct
