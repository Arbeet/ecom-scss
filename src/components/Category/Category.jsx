import React from 'react'
import Products from "../Products/Products"
import "./Category.scss"

function Category() {
  return (
    <div className='category-main-content'>
        <div className="layout">
            <div className="category-title">Category Title
            <Products innerPage={true} />
            </div>
        </div>
    </div>
  )
}

export default Category