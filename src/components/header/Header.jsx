import React, { useEffect, useState } from 'react'
import { Context } from '../../utils/context'
import "./Header.scss"
import {TbSearch } from 'react-icons/tb'
import {AiOutlineHeart} from 'react-icons/ai'
import {CgShoppingCart} from 'react-icons/cg'
import Cart from '../Cart/Cart'

function Header() {
  const[scroll, setScroll] = useState(false)
  const[showCart, setShowCart] = useState(false)

  const handleScroll =()=>{
    const offset = window.scrollY
    if (offset>200){
      setScroll(true)
    }else{
      setScroll(false)
    }
  }
  useEffect(()=>{
    window.addEventListener('scroll', handleScroll)
  })
  return (
    <>
    
    <header className={`main-header ${scroll ? 'sticky-header': ''}`}>
      <div className="header-content">
        <ul className='left'>
          <li>Home</li>
          <li>About</li>
          <li>Categories</li>
        </ul>
        <div className="center">ChaudharyG</div>
        <div className="right">
          <TbSearch />
          <AiOutlineHeart/>
          <span className='cart-icon'>
            <CgShoppingCart onClick={()=>{setShowCart(!showCart)}} />
            <span>5</span>
          </span>
        </div>
      </div>
    </header>
    {showCart && <Cart setShowCart={setShowCart} />}
    </>
  )
}

export default Header
