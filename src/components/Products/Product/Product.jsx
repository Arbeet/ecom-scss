import React from 'react'
import "./Product.scss"

function Product() {
  return (
    <div className="product-card">
      <div className="thumbnail">
        <img src="/assets/products/earbuds-prod-1.webp" alt="" />
        </div>
      <div className="prod-details">
        <span className='name'>Product Name</span>
        <span className="price">$1000</span>
      </div>
    </div>
  )
}

export default Product
