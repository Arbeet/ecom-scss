import React from 'react'
import "./Banner.scss"

function Banner() {
  return (
    <div className='hero-banner'>
        <div className="content">
            <div className="text-content">
                <h1>SALES</h1>
                <p>This is the thing that has to be the most thing about all those thing
                     in the the a quick brown box jumps over the dog </p>
                <div className="ctas">
                    <div className='banner-cta'>Read More</div>
                    <div className='banner-cta v2'>Shop Now</div>
                </div>
            </div>
            <img src="assets/banner-img.png" alt="" className='banner-img'/>
        </div>
    </div>
  )
}

export default Banner
