import React from 'react'
import "./home.scss"
import Banner from './Banner/Banner'
import Products from '../components/Products/Products'
import Category from './Category/Category'


function Home() {
  return (
    <div>
      <Banner />
      <div className="main-content">
        <div className="layout">
          <Category />
          <Products />
        </div>
      </div>
    </div>
  )
}

export default Home
